# coding=utf-8

# /projects
# /projects/:id
# /projects/:id/keys
# /projects/:id/signup
# /projects/:id/login
# /projects/:id/users
# /projects/:id/user/:id

from flask import Flask
from flask import jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return "Index"


@app.route('/projects')
def list_projects():
    return jsonify([]), 200
